import { sms } from "tencentcloud-sdk-nodejs-sms";
import TxConfig from "./_";

interface ITxSmsConfig {
  secretId: string;
  secretKey: string;

  /**
   * 地域信息
   *
   * https://cloud.tencent.com/document/api/382/52071#.E5.9C.B0.E5.9F.9F.E5.88.97.E8.A1.A8
   */
  region?: string;
  /**
   * https://console.cloud.tencent.com/smsv2/csms-sign
   */
  SignName: string;
  /**
   * https://console.cloud.tencent.com/smsv2/app-manage
   */
  SdkAppId: string;
  /**
   * 模版 id
   *
   * https://console.cloud.tencent.com/smsv2/csms-template
   */
  TemplateId: string;

  /**
   * 原样返回
   */
  SessionContext?: string;

  ExtendCode?: string;
  SenderId?: string;
}
type ISend = {
  phone: string;
  param: string[];
};
const send = async ({ phone, param }: ISend) => {
  const config: ITxSmsConfig = {
    secretId: "",
    secretKey: "",
    SdkAppId: "1400704048",
    SignName: "秦爽个人简历网",
    TemplateId: "1469264",

    ...TxConfig,
  };
  const client = new sms.v20210111.Client({
    credential: {
      /* 为了保护密钥安全，建议将密钥设置在环境变量中或者配置文件中。
       * 硬编码密钥到代码中有可能随代码泄露而暴露，有安全隐患，并不推荐。
       * SecretId、SecretKey 查询: https://console.cloud.tencent.com/cam/capi */
      secretId: config.secretId,
      secretKey: config.secretKey,
    },
    /* 必填：地域信息，可以直接填写字符串ap-guangzhou，支持的地域列表参考 https://cloud.tencent.com/document/api/382/52071#.E5.9C.B0.E5.9F.9F.E5.88.97.E8.A1.A8 */
    region: config.region || "ap-guangzhou",
    /* 非必填:
     * 客户端配置对象，可以指定超时时间等配置 */
    profile: {
      /* SDK默认用TC3-HMAC-SHA256进行签名，非必要请不要修改这个字段 */
      signMethod: "HmacSHA256",
      httpProfile: {
        reqMethod: "POST", // 请求方法
        reqTimeout: 10, // 请求超时时间，默认60s
        /**
         * 指定接入地域域名，默认就近地域接入域名为 sms.tencentcloudapi.com ，也支持指定地域域名访问，例如广州地域的域名为 sms.ap-guangzhou.tencentcloudapi.com
         */
        endpoint: "sms.tencentcloudapi.com",
      },
    },
  });

  const {
    SdkAppId,
    SignName,
    TemplateId,
    SessionContext,
    ExtendCode,
    SenderId,
  } = config;

  const { PhoneNumberInfoSet } = await client.DescribePhoneNumberInfo({
    PhoneNumberSet: ["+86" + phone],
  });

  if (Array.isArray(PhoneNumberInfoSet)) {
    const [{ Code: PhoneStatus }] = PhoneNumberInfoSet;

    if (PhoneStatus.toLocaleUpperCase() !== "OK") {
      throw new Error("无效的手机号码：" + phone);
    }
  } else {
    if (/^(?:(?:\+|00)86)?1\d{10}$/.test(phone) === false) {
      throw new Error("无效的手机号码：" + phone);
    }
  }

  return client.SendSms({
    TemplateParamSet: param,
    PhoneNumberSet: [phone],

    SmsSdkAppId: SdkAppId,
    SignName,
    TemplateId,
    SessionContext,
    ExtendCode,
    SenderId,
  });
};

interface ITxSendSmsResponse {
  SendStatusSet: ITxSendSmsResponseInfo[];
  RequestId: string;
}
interface ITxSendSmsResponseInfo {
  SerialNo: string;
  /**
   * '+8617347652655'
   */
  PhoneNumber: string;
  Fee: number;
  SessionContext: string;
  Code: "Ok" | string; // 详见 SmsErrorCode（下） https://cloud.tencent.com/document/api/382/55981#6.-.E9.94.99.E8.AF.AF.E7.A0.81
  Message: string; // 'send success'
  IsoCode: string; // 'CN'
}

export const SmsErrorCode = {
  "FailedOperation.ContainSensitiveWord": "短信内容中含有敏感词",
  "FailedOperation.FailResolvePacket":
    "请求包解析失败，通常情况下是由于没有遵守 API 接口说明规范导致的，请参考 请求包体解析1004错误详解。",
  "FailedOperation.InsufficientBalanceInSmsPackage":
    "套餐包余量不足，请 购买套餐包。",
  // FailedOperation.JsonParseFail	解析请求包体时候失败。
  // FailedOperation.MarketingSendTimeConstraint	营销短信发送时间限制，为避免骚扰用户，营销短信只允许在8点到22点发送。
  // FailedOperation.PhoneNumberInBlacklist	手机号在免打扰名单库中，通常是用户退订或者命中运营商免打扰名单导致的，可联系 腾讯云短信小助手 解决。
  // FailedOperation.SignatureIncorrectOrUnapproved	签名未审批或格式错误。（1）可登录 短信控制台，核查签名是否已审批并且审批通过；（2）核查是否符合格式规范，签名只能由中英文、数字组成，要求2 - 12个字，若存在疑问可联系 腾讯云短信小助手。
  // FailedOperation.TemplateIncorrectOrUnapproved	模板未审批或内容不匹配。（1）可登录 短信控制台，核查模板是否已审批并审批通过；（2）核查是否符合 格式规范，若存在疑问可联系 腾讯云短信小助手。
  // FailedOperation.TemplateParamSetNotMatchApprovedTemplate	请求内容与审核通过的模板内容不匹配。请检查请求中模板参数的个数是否与申请的模板一致。若存在疑问可联系 腾讯云短信小助手。
  // FailedOperation.TemplateUnapprovedOrNotExist	模板未审批或不存在。可登录 短信控制台，核查模板是否已审批并审批通过。若存在疑问可联系 腾讯云短信小助手。
  // InternalError.OtherError	其他错误，请联系 腾讯云短信小助手 并提供失败手机号。
  // InternalError.RequestTimeException	请求发起时间不正常，通常是由于您的服务器时间与腾讯云服务器时间差异超过10分钟导致的，请核对服务器时间及 API 接口中的时间字段是否正常。
  // InternalError.RestApiInterfaceNotExist	不存在该 RESTAPI 接口，请核查 REST API 接口说明。
  // InternalError.SendAndRecvFail	接口超时或短信收发包超时，请检查您的网络是否有波动，或联系 腾讯云短信小助手 解决。
  // InternalError.SigFieldMissing	后端包体中请求包体没有 Sig 字段或 Sig 为空。
  // InternalError.SigVerificationFail	后端校验 Sig 失败。
  // InternalError.Timeout	请求下发短信超时，请参考 60008错误详解。
  // InternalError.UnknownError	未知错误类型。
  // InvalidParameterValue.ContentLengthLimit	请求的短信内容太长，短信长度规则请参考 国内短信内容长度计算规则。
  // InvalidParameterValue.IncorrectPhoneNumber	手机号格式错误。
  // InvalidParameterValue.ProhibitedUseUrlInTemplateParameter	禁止在模板变量中使用 URL。
  // InvalidParameterValue.SdkAppIdNotExist	SdkAppId 不存在。
  // InvalidParameterValue.TemplateParameterFormatError	验证码模板参数格式错误，验证码类模板，模板变量只能传入0 - 6位（包括6位）纯数字。
  // InvalidParameterValue.TemplateParameterLengthLimit	单个模板变量字符数超过12个，企业认证用户不限制单个变量值字数，您可以 变更实名认证模式，变更为企业认证用户后，该限制变更约1小时左右生效。
  // LimitExceeded.AppCountryOrRegionDailyLimit	业务短信国家/地区日下发条数超过设定的上限，可自行到控制台应用管理>基础配置下调整国际港澳台短信发送限制。
  // LimitExceeded.AppCountryOrRegionInBlacklist	业务短信国家/地区不在国际港澳台短信发送限制设置的列表中而禁发，可自行到控制台应用管理>基础配置下调整国际港澳台短信发送限制。
  // LimitExceeded.AppDailyLimit	业务短信日下发条数超过设定的上限 ，可自行到控制台调整短信频率限制策略。
  // LimitExceeded.AppGlobalDailyLimit	业务短信国际/港澳台日下发条数超过设定的上限，可自行到控制台应用管理>基础配置下调整发送总量阈值。
  // LimitExceeded.AppMainlandChinaDailyLimit	业务短信中国大陆日下发条数超过设定的上限，可自行到控制台应用管理>基础配置下调整发送总量阈值。
  // LimitExceeded.DailyLimit	短信日下发条数超过设定的上限 (国际/港澳台)，如需调整限制，可联系 腾讯云短信小助手。
  // LimitExceeded.DeliveryFrequencyLimit	下发短信命中了频率限制策略，可自行到控制台调整短信频率限制策略，如有其他需求请联系 腾讯云短信小助手。
  // LimitExceeded.PhoneNumberCountLimit	调用接口单次提交的手机号个数超过200个，请遵守 API 接口输入参数 PhoneNumberSet 描述。
  // LimitExceeded.PhoneNumberDailyLimit	单个手机号日下发短信条数超过设定的上限，可自行到控制台调整短信频率限制策略。
  // LimitExceeded.PhoneNumberOneHourLimit	单个手机号1小时内下发短信条数超过设定的上限，可自行到控制台调整短信频率限制策略。
  // LimitExceeded.PhoneNumberSameContentDailyLimit	单个手机号下发相同内容超过设定的上限，可自行到控制台调整短信频率限制策略。
  // LimitExceeded.PhoneNumberThirtySecondLimit	单个手机号30秒内下发短信条数超过设定的上限，可自行到控制台调整短信频率限制策略。
  // MissingParameter.EmptyPhoneNumberSet	传入的号码列表为空，请确认您的参数中是否传入号码。
  // UnauthorizedOperation.IndividualUserMarketingSmsPermissionDeny	个人用户没有发营销短信的权限，请参考 权益区别。
  // UnauthorizedOperation.RequestIpNotInWhitelist	请求 IP 不在白名单中，您配置了校验请求来源 IP，但是检测到当前请求 IP 不在配置列表中，如有需要请联系 腾讯云短信小助手。
  // UnauthorizedOperation.RequestPermissionDeny	请求没有权限，请联系 腾讯云短信小助手。
  // UnauthorizedOperation.SdkAppIdIsDisabled	此 SdkAppId 禁止提供服务，如有需要请联系 腾讯云短信小助手。
  // UnauthorizedOperation.SerivceSuspendDueToArrears	欠费被停止服务，可自行登录腾讯云充值来缴清欠款。
  // UnauthorizedOperation.SmsSdkAppIdVerifyFail	SmsSdkAppId 校验失败，请检查 SmsSdkAppId 是否属于 云API密钥 的关联账户。
  // UnsupportedOperation.	不支持该请求。
  // UnsupportedOperation.ChineseMainlandTemplateToGlobalPhone	国内短信模板不支持发送国际/港澳台手机号。发送国际/港澳台手机号请使用国际/港澳台短信正文模板。
  // UnsupportedOperation.ContainDomesticAndInternationalPhoneNumber	群发请求里既有国内手机号也有国际手机号。请排查是否存在（1）使用国内签名或模板却发送短信到国际手机号；（2）使用国际签名或模板却发送短信到国内手机号。
  // UnsupportedOperation.GlobalTemplateToChineseMainlandPhone	国际/港澳台短信模板不支持发送国内手机号。发送国内手机号请使用国内短信正文模板。
  // UnsupportedOperation.UnsuportedRegion	不支持该地区短信下发。
};

send({
  phone: "17347652655",
  param: ["3333"],
})
  .then(console.log)
  .catch(console.error);
