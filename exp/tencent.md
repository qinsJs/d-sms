# 腾讯云短信服务

[短信业务 API 文档](https://cloud.tencent.com/document/product/382/52077)  
[Node.js 示例代码](https://cloud.tencent.com/document/product/382/43197)

个人认证用户不支持使用签名、正文模板相关接口

腾讯 SMS 的三个概念：应用，签名，模版

应用 提供 SDK AppID，API 调用需要

secretId 和 secretKey 在 “访问管理” 里面可以找到。
