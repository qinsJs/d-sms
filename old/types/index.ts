export type PhoneNumber = string;

export interface IServerResponse<T> {
  success: boolean;
  data: T;
}
export interface ISendSmsResponse<T> extends IServerResponse<T> {
  messages: string[];
  phones: boolean[];
}

export * from "./tencent";
