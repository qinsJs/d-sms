import { IApiTencentSendSmsConfig, PhoneNumber } from "../index";

export interface ISendSmsReqData {
  tag: "tencent" | "tx" | "aliyun" | "ali";
  phones: PhoneNumber[];
  config: IApiTencentSendSmsConfig;
}
