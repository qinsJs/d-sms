import Koa from "koa";
import { ISendSmsResponse } from "my-types";

export const okk = (ctx: Koa.Context, data: ISendSmsResponse<any>) => {
  ctx.body = data;
};
