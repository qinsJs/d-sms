import Koa from "koa";
import { ISendSmsReqData } from "my-types/api/send";
import { tencentSendSms } from "./tencent/tencent";

export const sendShortMessage = async (ctx: Koa.Context, next: Koa.Next) => {
  const req: ISendSmsReqData = ctx.request.body;
  const { phones, tag, config } = req;
  if (!phones) {
    throw new Error("下发手机号码（phones） 不能为空");
  }
  if (!Array.isArray(phones)) {
    req.phones = [phones];
  }

  if (!config) {
    throw new Error("配置（config） 不能为空");
  }

  console.log("tag", "=", tag);

  switch (tag) {
    case "ali":
    case "aliyun": {
      throw new Error("todo");
    }

    case "tencent":
    case "tx":
      return tencentSendSms(ctx, next);
    default:
      break;
  }

  throw new Error("无效的 tag : " + req.tag);
};
