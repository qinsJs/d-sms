import Koa from "koa";
import * as path from "path";
import * as fs from "fs";
import { lookup } from "mime-types";

const webAssetsPath =
  process.env.NODE_ENV === "dev" ? "../../../code/web" : "./web";

const webDist = (file: string) => path.join(__dirname, webAssetsPath, file);

export const webAssets = async (ctx: Koa.Context) => {
  const indexof = ctx.path.indexOf("assets");
  const dir = webDist(ctx.path.substring(indexof));

  ctx.set("Content-Type", lookup(dir) || "text/html;charset=utf-8");
  ctx.body = fs.readFileSync(dir);
};

export const pageIndex = async (ctx: Koa.Context) => {
  const dir = webDist("index.html");

  ctx.set("Content-Type", "text/html;charset=utf-8");
  ctx.body = fs.readFileSync(dir);
};

export const page404 = async (ctx: Koa.Context) => {
  pageIndex(ctx);
  ctx.status = 404;
};

export const pagex = async (ctx: Koa.Context) => {
  if (lookup(ctx.path)) {
    return webAssets(ctx);
  }

  ctx.redirect("/404");
};
