import { sms } from "tencentcloud-sdk-nodejs-sms";
import { ITencentClient } from "my-types";

const smsClient = sms.v20210111.Client;

export const createTencentSmsClient = ({
  auth,
  region,
  reqTimeout,
}: ITencentClient) => {
  interface hehe {
    secretId?: string;
    secretKey?: string;
    token?: string;
  }
  const credential: hehe = {};

  if (typeof auth === "object") {
    const { secretId, secretKey } = auth;
    credential.secretId = secretId;
    credential.secretKey = secretKey;
  } else if (typeof auth === "string") {
    credential.token = auth;
  } else {
    throw new Error("缺少认证参数");
  }

  return new smsClient({
    credential,
    /* 必填：地域信息，可以直接填写字符串ap-guangzhou，支持的地域列表参考 https://cloud.tencent.com/document/api/382/52071#.E5.9C.B0.E5.9F.9F.E5.88.97.E8.A1.A8 */
    region: region ?? "ap-guangzhou",
    /* 非必填:
     * 客户端配置对象，可以指定超时时间等配置 */
    profile: {
      /* SDK默认用TC3-HMAC-SHA256进行签名，非必要请不要修改这个字段 */
      signMethod: "HmacSHA256",
      httpProfile: {
        /* SDK默认使用POST方法。
         * 如果你一定要使用GET方法，可以在这里设置。GET方法无法处理一些较大的请求 */
        reqMethod: "POST",
        /* SDK有默认的超时时间，非必要请不要进行调整
         * 如有需要请在代码中查阅以获取最新的默认值 */
        reqTimeout: reqTimeout ?? 30,
        /**
         * 指定接入地域域名，默认就近地域接入域名为 sms.tencentcloudapi.com ，也支持指定地域域名访问，例如广州地域的域名为 sms.ap-guangzhou.tencentcloudapi.com
         */
        endpoint: "sms.tencentcloudapi.com",
      },
    },
  });
};
