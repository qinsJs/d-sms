import { ITencentSendSmsParam } from "my-types";

interface ISendSmsParam extends ITencentSendSmsParam {
  /* 下发手机号码 */
  PhoneNumberSet: string[];
}

export const createSendSmsParam = ({
  SmsSdkAppId,
  SignName,
  TemplateId,
  TemplateParamSet,
  PhoneNumberSet,
  SessionContext,
}: ISendSmsParam) => ({
  SmsSdkAppId,
  SignName,
  TemplateId,
  TemplateParamSet: TemplateParamSet ?? [],
  PhoneNumberSet,
  SessionContext: SessionContext ?? "",
  /* 短信码号扩展号（无需要可忽略）: 默认未开通，如需开通请联系 [腾讯云短信小助手] */
  ExtendCode: "",
  /* 国际/港澳台短信 senderid（无需要可忽略）: 国内短信填空，默认未开通，如需开通请联系 [腾讯云短信小助手] */
  SenderId: "",
});
