import Koa from "koa";
import {
  PhoneNumber,
  IApiTencentSendSmsConfig,
  ISendSmsResponse,
  ITencentSendSmsResponse,
} from "my-types";

import { createTencentSmsClient } from "./SmsClient";
import { createSendSmsParam } from "./SendSms";
import { ISendSmsReqData } from "my-types/api/send";
import { okk } from "../nok";
// import { SendSmsResponse } from "tencentcloud-sdk-nodejs-sms/tencentcloud/services/sms/v20190711/sms_models";

const sendSms = async (
  phones: PhoneNumber[],
  config: IApiTencentSendSmsConfig
) => {
  const { client, param } = config;
  const txClient = createTencentSmsClient(client);
  const params = createSendSmsParam({ ...param, PhoneNumberSet: phones });
  return txClient.SendSms(params);
};

export const tencentSendSms = async (ctx: Koa.Context, next: Koa.Next) => {
  const req: ISendSmsReqData = ctx.request.body;
  const { phones, config } = req;

  if (!config.param) {
    throw new Error("腾讯云SMS配置（config.param） 不能为空");
  }
  if (!config.param) {
    throw new Error("下发手机号码（phones） 不能为空");
  }
  if (!config.param.SmsSdkAppId) {
    throw new Error("短信应用ID（SmsSdkAppId） 不能为空");
  }

  if (!config.param.SignName) {
    throw new Error("短信签名内容（SignName） 不能为空");
  }

  if (!config.param.TemplateId) {
    throw new Error("模板ID（TemplateId） 不能为空");
  }

  const res = await sendSms(phones, config);

  const result: ISendSmsResponse<ITencentSendSmsResponse> = {
    success: false,
    phones: [],
    messages: [],
    data: res,
  };

  try {
    res.SendStatusSet.forEach(({ Code, Message }) => {
      const isSuccess =
        Code && Code.toLocaleUpperCase() === "OK" ? true : false;
      result.phones.push(isSuccess);
      result.messages.push(Message);
    });
    result.success = phones.length > 0 && phones.every((bool) => bool);
  } catch (error) {
    if (error instanceof Error) {
      result.messages.push(error.message);
    }
  }

  okk(ctx, result);
};
