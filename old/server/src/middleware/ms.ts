import Koa from "koa";

export const Middleware__MS = async (ctx: Koa.Context, next: Koa.Next) => {
  if (!ctx.path.startsWith("/api")) return next();
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
};
