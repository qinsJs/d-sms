import Koa from "koa";

export const Middleware__Error = async (ctx: Koa.Context, next: Koa.Next) => {
  try {
    await next();
  } catch (error) {
    console.error(`服务器错误：`, error);

    if (!error || !(error instanceof Error)) return;

    const stack = error?.stack?.split("\n");

    ctx.status = 500;
    ctx.body = {
      code: 500,
      message: error.message || "服务器错误",
      stack,
    };
  }
};
