import Koa from "koa";
import KoaRouter from "@koa/router";
import { koaBody } from "koa-body";

import { Middleware__Error } from "./middleware/error";
import { Middleware__MS } from "./middleware/ms";

import { sendShortMessage } from "./api/send";

import { pageIndex, page404, pagex, webAssets } from "./api/html";

const router = new KoaRouter();

router.post("/api/send", sendShortMessage);

router.get("/", pageIndex);
router.get("/assets/(.*)", webAssets);
router.get("/404", page404);
router.get("/(.*)", pagex);

const app = new Koa();

app.use(Middleware__Error);
app.use(Middleware__MS);
app.use(koaBody());
app.use(router.routes());

app.listen(9000, () => {
  console.log("服务启动成功");
});
