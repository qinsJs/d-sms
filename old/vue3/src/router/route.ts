import { createRouter, createWebHistory } from "vue-router";

const routerHistory = createWebHistory();

const route = createRouter({
  history: routerHistory,
  routes: [
    {
      path: "/",
      redirect: "/home",
    },
    {
      path: "/home",
      component: () => import("../pages/Home/Home.vue"),
    },
    {
      path: "/404",
      component: () => import("../pages/p404/p404.vue"),
    },
    {
      path: "/:pathMatch(.*)",
      redirect: "/404",
    },
  ],
});

export { route };
