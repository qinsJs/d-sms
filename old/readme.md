# 发送短信 / Send Short Message

`server` 为服务端项目，koa + 云厂商依赖

`vue3` 为前端展示部分

server 通过 [ncc](https://www.npmjs.com/package/@vercel/ncc) 打包成一个 index.js

最终通过 serverless-devs 部署到 阿里云 FC 上

部署：

```
./deploy
```

## 其他

- [TypeScript tsconfig 配置详解](https://juejin.cn/post/6844904093568221191)
