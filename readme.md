# 短信

短信本质上 是身份的验证。

手机号，业务用途（tag），只有你知我知的随机验证码

短信的实效时间应该灵活，比如有的业务要求 10 分钟实效，有的业务要求 30 分钟实效。

```ts
export class SMS {
  id: string;
  tag: string;
  phone: string;
  code: string;
  close: false;
  useAt: Date;
  remark?: string;
}
```

发送记录，以及重新发送的限制需要考虑 db 进行设计。
